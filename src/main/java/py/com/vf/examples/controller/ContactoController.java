package py.com.vf.examples.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.vf.examples.bean.Contacto;
import py.com.vf.examples.repository.ContactoRespository;

@RestController
@RequestMapping("/contactos")
public class ContactoController {

	@Autowired
	private ContactoRespository contactoRepository;
	
	@GetMapping
	public List<Contacto> obtenerContactos() {
		List<Contacto> contactos = contactoRepository.obtenerContactos();
		
		if (contactos.size() > 0) { 
			//TODO: Lanzar exception not found exception
		}
		return contactos;
	}
	
	@GetMapping("/{id}")
	public Contacto obtenerContacto(@PathVariable("id") String id) {
		Contacto contacto = contactoRepository.obtenerContacto(Integer.valueOf(id));
		
		if (contacto == null) { 
			//TODO: Lanzar exception not found exception
		}
		return contacto;
	}
	
	@PostMapping
	public ResponseEntity<Contacto> guardarContacto(@RequestBody Contacto contacto, UriComponentsBuilder uBuilder) {
		Contacto c = contactoRepository.agregarContacto(contacto);
		HttpHeaders header = new HttpHeaders();
		
		URI uri = uBuilder.path("/contactos/" )
				.path(String.valueOf(contacto.getId()))
				.build()
				.toUri();
		
		header.setLocation(uri);
		
		return new ResponseEntity<>(c, header, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public Contacto editarContacto(@PathVariable("id") String id,
			@RequestBody Contacto contacto, 
			UriComponentsBuilder uBuilder) 
	{
		Contacto c = null;
		boolean result = contactoRepository.actualizarContacto(contacto, Integer.valueOf(id));
		if (result) {
			 c = obtenerContacto(id);
		}
		return c;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarContacto(@PathVariable("id") String id) { 
		
		boolean delete = contactoRepository.eliminarContacto(Integer.valueOf(id));
		return new ResponseEntity<Contacto> (HttpStatus.NO_CONTENT);
	}
}
