package py.com.vf.examples.repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import py.com.vf.examples.bean.Contacto;

@Repository
public class ContactoRepositoryImpl implements ContactoRespository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public List<Contacto> obtenerContactos() {
		List<Contacto> contactos = jdbcTemplate.query("select * from contactos", 
				(rs) -> {
					List<Contacto> list = new ArrayList<>();
					while(rs.next()){
						Contacto c = new Contacto();
						c.setId(rs.getInt("id"));
						c.setNombre(rs.getString("nombre"));
						c.setMail(rs.getString("mail"));
						c.setDireccion(rs.getString("direccion"));
						list.add(c);
					}
					return list;
				});
		return contactos;
	}

	@Override
	public Contacto agregarContacto(Contacto contacto) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into contactos values (?,?,?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setNull(1, Types.INTEGER);
			ps.setString(2, contacto.getNombre());
			ps.setString(3,  contacto.getMail());
			ps.setString(4, contacto.getDireccion());
	        return ps;
		}; 
		
		int result = jdbcTemplate.update(psc, keyHolder);
		
		if (result > 0)
			contacto.setId(keyHolder.getKey().intValue());
		
		return contacto;
		
	}

	@Override
	public boolean eliminarContacto(Integer id) {
		int result = jdbcTemplate.update("delete from contactos where id = ?", id);
		
		return (result > 0) ? true : false;
	}

	@Override
	public boolean actualizarContacto(Contacto contacto, Integer id) {
		int result = jdbcTemplate.update("update contactos set nombre = ?, mail = ?, direccion = ? where id = ?", 
				contacto.getNombre(), contacto.getMail(), contacto.getDireccion(), id);
		return (result > 0) ? true : false;
	}

	@Override
	public Contacto obtenerContacto(Integer id) {
		Contacto contacto = jdbcTemplate.queryForObject("select * from contactos where id = ?", 
				new Object[] {id}, 
				(rs, rowNum) -> {
					Contacto c = new Contacto();
					c.setId(rs.getInt("id"));
					c.setNombre(rs.getString("nombre"));
					c.setMail(rs.getString("mail"));
					c.setDireccion(rs.getString("direccion"));
					return c;
		        });
		
		return contacto;
	}

}
